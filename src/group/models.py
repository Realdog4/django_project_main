from django.core.validators import MaxValueValidator, MinValueValidator, EmailValidator
from django.db import models
from faker import Faker


class Group(models.Model):
    group_number = models.PositiveSmallIntegerField(default=1, null=True, blank=True, validators=[MinValueValidator(1)])
    email_group = models.EmailField(max_length=120, null=True, blank=True, validators=[EmailValidator()])
    subject = models.CharField(max_length=100, default="No subject")
    students_count = models.PositiveSmallIntegerField(
        default=0, null=True, blank=True, validators=[MinValueValidator(1), MaxValueValidator(50)]
    )
    start_date = models.DateField(null=True, blank=True)
    description = models.TextField(default="No description", null=True, blank=True)

    class Meta:
        verbose_name = "Group list"
        verbose_name_plural = "All groups"
        ordering = ("group_number",)

    @classmethod
    def generate_instances(cls, count):
        faker = Faker()
        for _ in range(count):
            cls.objects.create(
                group_number=faker.random_int(min=1, max=1000),
                email_group=faker.email(),
                subject=faker.job(),
                students_count=faker.random_int(min=1, max=30),
                start_date=faker.date_between(start_date="-30d", end_date="+30d"),
                description=faker.sentence(nb_words=10, variable_nb_words=False),
            )

    def __str__(self):
        return f"{self.group_number} - {self.subject}"
