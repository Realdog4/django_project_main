from django.urls import path
from group.views import GroupListView, GroupCreate, GroupUpdateView, GroupDeleteView, GroupDetailView

app_name = "groups"

urlpatterns = [
    path("", GroupListView.as_view(), name="group_list"),
    path("create/", GroupCreate.as_view(), name="group_create"),
    path("update/<int:pk>/", GroupUpdateView.as_view(), name="update_group"),
    path("delete/<int:pk>/", GroupDeleteView.as_view(), name="delete_group"),
    path("details/<int:pk>/", GroupDetailView.as_view(), name="details_group"),
]
