from django.contrib import admin
from .models import Group


@admin.register(Group)
class GroupAdmin(admin.ModelAdmin):
    list_display = ("group_number", "subject", "email_group", "start_date", "description", "count_students_in_group")
    list_display_links = ("email_group", "group_number")
    search_fields = ("group_number", "subject")
    list_filter = ("group_number", "start_date")

    def count_students_in_group(self, instance):
        return instance.student_set.count()