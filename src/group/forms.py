from django.core.exceptions import ValidationError
from django.forms import ModelForm

from group.models import Group


class GroupForm(ModelForm):
    class Meta:
        model = Group
        fields = ["group_number", "email_group", "subject", "students_count", "start_date", "description"]

    @staticmethod
    def normalize_text(text: str) -> str:
        return text.strip().capitalize()

    def clean_email(self):
        email_group = self.cleaned_data["email_group"]

        if "@yandex" in email_group.lower():
            raise ValidationError("Yandex is not a valid email address!")
        return email_group

    def clean_subject(self):
        return self.normalize_text(self.cleaned_data["subject"])
