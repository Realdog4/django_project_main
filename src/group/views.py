from django.http import HttpResponseRedirect
from django.views.generic import ListView, CreateView, UpdateView, DetailView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy

from .forms import GroupForm
from .models import Group


class GroupListView(ListView):
    model = Group
    template_name = "groups/group_list.html"
    context_object_name = "groups"


class GroupCreate(LoginRequiredMixin, CreateView):
    template_name = "groups/group_create.html"
    form_class = GroupForm
    success_url = reverse_lazy("groups:group_list")


class GroupUpdateView(LoginRequiredMixin, UpdateView):
    template_name = "groups/group_update.html"
    form_class = GroupForm
    success_url = reverse_lazy("groups:group_list")
    queryset = Group.objects.all()


class GroupDeleteView(LoginRequiredMixin, UpdateView):
    model = Group
    template_name = "groups/group_delete.html"
    form_class = GroupForm
    success_url = reverse_lazy("groups:group_list")
    pk_url_kwarg = 'pk'

    def form_valid(self, form):
        group = form.instance
        group.delete()
        return HttpResponseRedirect(self.success_url)


class GroupDetailView(DetailView):
    model = Group
    template_name = 'groups/group_details.html'
    context_object_name = 'group'
    form_class = GroupForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        form = self.form_class(instance=self.object)
        context["form"] = form
        return context
