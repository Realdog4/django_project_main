from django.contrib import admin
from django.urls import path, include
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

import group.urls
import students.urls
import teachers.urls
from core.views import (IndexView, Custom404View, UserLoginView,
                        UserLogoutView, UserRegistrationView, send_test_email,
                        ActivateUserView, UserProfileView)
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    path("", IndexView.as_view(), name="index"),
    path("__debug__/", include("debug_toolbar.urls")),
    path('jet/', include('jet.urls', 'jet')),
    path("admin/", admin.site.urls),
    path("login/", UserLoginView.as_view(), name="login"),
    path("logout/", UserLogoutView.as_view(), name="logout"),
    path("register/", UserRegistrationView.as_view(), name="register"),
    path("activate/<str:uuid64>/<str:token>/", ActivateUserView.as_view(), name="activate_user"),
    path('profile/<int:pk>/', UserProfileView.as_view(), name='user_profile'),
    path("send_test_email/", send_test_email, name="send_test_email"),
    path("students/", include(students.urls)),
    path("teachers/", include(teachers.urls)),
    path("groups/", include(group.urls)),
    path("oauth/", include("social_django.urls", namespace="social")),
]

urlpatterns += staticfiles_urlpatterns()

handler404 = Custom404View.as_view()

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
