from django.contrib import admin
from .models import Teacher
from django.urls import reverse
from django.utils.html import format_html


@admin.register(Teacher)
class TeacherAdmin(admin.ModelAdmin):
    list_display = ("first_name", "last_name", "email", "birth_date", "qualification", "group_count", "links_to_groups")
    list_display_links = ("email", "first_name")
    search_fields = ("first_name", "last_name")
    fieldsets = (
        ("Personal info", {"fields": ("first_name", "last_name", "email", "qualification")}),
        (
            "Additional info",
            {
                "classes": ("collapse",),
                "fields": (
                    "birth_date",
                    "group",
                ),
            },
        ),
    )

    def group_count(self, instance):
        if instance.group:
            return instance.group.count()
        return 0

    def links_to_groups(self, instance):
        if instance.group:
            groups = instance.group.all()
            links = []
            for group in groups:
                links.append(
                    f"<a class='button' href='{reverse('admin:group_group_change', args=[group.pk])}'"
                    f">{group.group_number}</a>"
                )
            return format_html(" </span>".join(links))
        return "No groups found"


