from django.urls import path
from teachers.views import TeacherListView, TeacherCreate, TeacherUpdateView, TeacherDeleteView

app_name = "teachers"

urlpatterns = [
    path("", TeacherListView.as_view(), name="teachers_list"),
    path("create/", TeacherCreate.as_view(), name="teacher_create"),
    path("update/<int:pk>/", TeacherUpdateView.as_view(), name="update_teacher"),
    path("delete/<int:pk>/", TeacherDeleteView.as_view(), name="delete_teacher"),
]
