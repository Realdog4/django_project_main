from random import choice

from django.db import models
from faker import Faker

from core.models import Person

from group.models import Group


class Teacher(Person):
    phone_number = models.CharField(max_length=20)
    qualification = models.CharField(max_length=100)
    group = models.ManyToManyField(to="group.Group")

    class Meta:
        verbose_name = "Teacher list"
        verbose_name_plural = "All teachers"
        ordering = ("first_name",)

    @classmethod
    def generate_instances(cls, count):
        faker = Faker()
        groups = Group.objects.all()
        for _ in range(count):
            teacher = cls.objects.create(
                avatar="img/user.png",
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                email=faker.email(),
                birth_date=faker.date_of_birth(),
                phone_number=faker.phone_number(),
                qualification=faker.job(),
            )
            teacher.group.set([choice(groups)])
