from django.http import HttpResponseRedirect
from django.views.generic import ListView, CreateView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy

from .forms import TeacherForm
from .models import Teacher


class TeacherListView(ListView):
    model = Teacher
    template_name = "teachers/teachers_list.html"
    context_object_name = "teachers"


class TeacherCreate(LoginRequiredMixin, CreateView):
    template_name = "teachers/teachers_create.html"
    form_class = TeacherForm
    success_url = reverse_lazy("teachers:teachers_list")


class TeacherUpdateView(LoginRequiredMixin, UpdateView):
    template_name = "teachers/teachers_update.html"
    form_class = TeacherForm
    success_url = reverse_lazy("teachers:teachers_list")
    queryset = Teacher.objects.all()


class TeacherDeleteView(LoginRequiredMixin, UpdateView):
    model = Teacher
    template_name = "teachers/teachers_delete.html"
    form_class = TeacherForm
    success_url = reverse_lazy("teachers:teachers_list")
    pk_url_kwarg = 'pk'

    def form_valid(self, form):
        teacher = form.instance
        teacher.delete()
        return HttpResponseRedirect(self.success_url)
