from django.core.exceptions import ValidationError
from django.forms import ModelForm

from teachers.models import Teacher


class TeacherForm(ModelForm):
    class Meta:
        model = Teacher
        fields = ["avatar", "first_name", "last_name", "email", "birth_date", "phone_number", "qualification", "group"]

    @staticmethod
    def normalize_text(text: str) -> str:
        return text.strip().capitalize()

    def clean_email(self):
        email = self.cleaned_data["email"]

        if "@yandex" in email.lower():
            raise ValidationError("Yandex is not a valid email address!")
        return email

    def clean_subject(self):
        return self.normalize_text(self.cleaned_data["first_name", "last_name", "qualification"])
