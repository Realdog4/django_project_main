from django.db.models import Q
from django.http import HttpResponseRedirect
from django.views.generic import ListView, CreateView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy

from students.forms import StudentForm
from students.models import Student


class StudentListView(ListView):
    model = Student
    template_name = "students/students_list.html"
    context_object_name = "students"

    def get_queryset(self):
        queryset = super().get_queryset()
        search_query = self.request.GET.get('search')
        search_fields = ["first_name", "last_name", "email"]

        if search_query:
            or_filter = Q()
            for field in search_fields:
                or_filter |= Q(**{f"{field}__icontains": search_query})
            queryset = queryset.filter(or_filter)

        return queryset


class StudentCreate(LoginRequiredMixin, CreateView):
    template_name = "students/student_create.html"
    form_class = StudentForm
    success_url = reverse_lazy("students:get_students")


class StudentUpdateView(LoginRequiredMixin, UpdateView):
    template_name = "students/student_update.html"
    form_class = StudentForm
    success_url = reverse_lazy("students:get_students")
    queryset = Student.objects.all()


class StudentDeleteView(LoginRequiredMixin, UpdateView):
    model = Student
    template_name = "students/student_delete.html"
    form_class = StudentForm
    success_url = reverse_lazy("students:get_students")
    pk_url_kwarg = 'pk'

    def form_valid(self, form):
        student = form.instance
        student.delete()
        return HttpResponseRedirect(self.success_url)
