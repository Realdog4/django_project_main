from django.urls import path
from .views import StudentListView, StudentCreate, StudentUpdateView, StudentDeleteView

app_name = "students"

urlpatterns = [
    path("", StudentListView.as_view(), name="get_students"),
    path("create/", StudentCreate.as_view(), name="create_student"),
    path("update/<int:pk>/", StudentUpdateView.as_view(), name="update_student"),
    path("delete/<int:pk>/", StudentDeleteView.as_view(), name="delete_student"),
]
