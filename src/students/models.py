import datetime

from django.db import models
from faker import Faker
from django.core.validators import FileExtensionValidator

from core.models import Person
from core.utils.validators import validate_resume
from group.models import Group


class Student(Person):

    grade = models.PositiveSmallIntegerField(null=True, blank=True)
    group = models.ForeignKey(to="group.Group", on_delete=models.CASCADE)
    resume = models.FileField(upload_to="resumes/", null=True, blank=True,
                              validators=[FileExtensionValidator(allowed_extensions=['pdf', 'doc', 'docx']),
                                          validate_resume])

    class Meta:
        verbose_name = "Student list"
        verbose_name_plural = "All students"
        ordering = ("first_name",)

    @classmethod
    def generate_instances(cls, count):
        faker = Faker()
        for _ in range(count):
            cls.objects.create(
                avatar="img/user.png",
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                email=faker.email(),
                birth_date=faker.date_of_birth(minimum_age=18, maximum_age=60),
                group=Group.objects.get(id=1)
            )

    def age(self):
        return datetime.datetime.now().year - self.birth_date.year

    def __str__(self):
        return f"{self.first_name} {self.last_name} {self.email} ({self.pk})"
