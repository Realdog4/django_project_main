from django.contrib import admin
from .models import Student


@admin.register(Student)
class StudentAdmin(admin.ModelAdmin):
    ordering = (
        "first_name",
        "group__group_number",
    )
    list_display = ("first_name", "last_name", "email", "age", "birth_date")
    list_display_links = ("email", "first_name")
    list_filter = ("birth_date", "group__group_number")
    date_hierarchy = "birth_date"
    search_fields = ("first_name__istartswith",)

