from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.core.validators import MinLengthValidator
from django.db import models

from .managers import CustomerManager
from .utils.validators import first_name_validator
from django.contrib.auth import get_user_model
from phonenumber_field.modelfields import PhoneNumberField
from django.utils.translation import gettext_lazy as _
from django.utils import timezone


class Person(models.Model):
    avatar = models.ImageField(upload_to="img/", null=True, blank=True)
    first_name = models.CharField(
        max_length=120, null=True, blank=True, validators=[MinLengthValidator(2), first_name_validator]
    )
    last_name = models.CharField(max_length=120, null=True, blank=True)
    email = models.EmailField(max_length=150, null=True, blank=True)
    birth_date = models.DateField(null=True, blank=True)

    class Meta:
        abstract = True


class Customer(AbstractBaseUser, PermissionsMixin):
    first_name = models.CharField(_("Name"), max_length=150, blank=True)
    last_name = models.CharField(_("Surname"), max_length=150, blank=True)
    email = models.EmailField(
        _("email address"),
        unique=True,
        error_messages={
            "unique": _("A user with that email already exists."),
        },
    )
    is_staff = models.BooleanField(
        _("staff status"),
        default=False,
        help_text=_("Designates whether the user can log into this admin site."),
    )
    is_active = models.BooleanField(
        _("active"),
        default=True,
        help_text=_(
            "Designates whether this user should be treated as active. " "Unselect this instead of deleting accounts."
        ),
    )
    date_joined = models.DateTimeField(_("date joined"), default=timezone.now)
    department = models.CharField(_("department"), max_length=300, null=True, blank=True)
    birth_day = models.DateField(_("birth day"), blank=True, null=True)
    avatar = models.ImageField(upload_to="static/img/profiles", null=True)
    objects = CustomerManager()

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = _("Customer list")
        verbose_name_plural = _("Customers")

    def clean(self):
        super().clean()
        self.email = self.__class__.objects.normalize_email(self.email)

    def get_full_name(self):
        """
        Return the first_name plus the last_name, with a space in between.
        """
        full_name = "%s %s" % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        """Return the short name for the user."""
        return self.first_name

    def get_period_of_registration(self):
        return f"Time on site: {timezone.now() - self.date_joined}"


class UserProfile(models.Model):
    USER_TYPES = (
        ('Teacher', 'Teacher'),
        ('Student', 'Student'),
        ('Mentor', 'Mentor'),
    )

    user = models.OneToOneField(get_user_model(), on_delete=models.CASCADE)
    birth_date = models.DateField(null=True, blank=True)
    phone_number = PhoneNumberField()
    photo = models.ImageField(upload_to="static/img/profiles", default=None, blank=True, null=True)
    location = models.CharField(max_length=255, blank=True, null=True)
    user_type = models.CharField(max_length=10, choices=USER_TYPES)
    bio = models.TextField(blank=True)

    def __str__(self):
        return str(self.user)