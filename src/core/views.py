from django.views import View
from django.views.generic import TemplateView, CreateView, RedirectView, UpdateView
from django.contrib.auth.views import LoginView, LogoutView
from django.shortcuts import render
from .forms import UserRegistrationForm, UserProfileForm
from django.urls import reverse_lazy
from django.contrib.auth import login, get_user_model
from django.core.mail import send_mail
from datetime import datetime
from django.http import HttpResponse
from django.conf import settings
from .services.emails import send_registration_email
from django.utils.encoding import force_str
from django.utils.http import urlsafe_base64_decode
from .utils.token_generator import TokenGenerator
from .models import UserProfile, Customer


class IndexView(TemplateView):
    template_name = "index.html"
    http_method_names = ['get']


class Custom404View(View):
    def get(self, request, exception=None):
        return render(request, '404.html', status=404)


class UserRegistrationView(CreateView):
    template_name = "registration/create_user.html"
    form_class = UserRegistrationForm
    success_url = reverse_lazy("index")

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.is_active = False
        self.object.save()

        UserProfile.objects.create(user=self.object)

        send_registration_email(request=self.request, user_instance=self.object)

        return super().form_valid(form)


class ActivateUserView(RedirectView):
    url = reverse_lazy("index")

    def get(self, request, uuid64, token, *args, **kwargs):
        try:
            pk = force_str(urlsafe_base64_decode(uuid64))
            current_user = get_user_model().objects.get(pk=pk)
        except (get_user_model().DoesNotExist, TypeError, ValueError):
            return HttpResponse("Wrong data!!!")

        if current_user and TokenGenerator().check_token(current_user, token):
            current_user.is_active = True
            current_user.save()

            login(request, current_user)

            return super().get(request, *args, **kwargs)

        return HttpResponse("Wrong data!!!")


class UserLoginView(LoginView):
    ...


class UserLogoutView(LogoutView):
    ...


def send_test_email(request):
    current_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    message_body = f"Tsehychko Bogdan, current time is: {current_time}"

    send_mail(
        subject="Tsehychko Bogdan",
        message=message_body,
        from_email=settings.EMAIL_HOST_USER,
        recipient_list=[
            settings.EMAIL_HOST_USER,
            "mikolaz2727@gmail.com",
        ],
    )
    return HttpResponse("Done")


class UserProfileView(UpdateView):
    model = UserProfile
    template_name = 'user/profile.html'
    form_class = UserProfileForm
    success_url = reverse_lazy("index")
    queryset = Customer.objects.all()

    def get_object(self, queryset=None):
        return self.request.user.userprofile

    def form_invalid(self, form):
        return super().form_invalid(form)

    def form_valid(self, form):
        form.instance.user = self.request.user
        form.save()
        return super().form_valid(form)
