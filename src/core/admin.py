from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin
from .models import UserProfile


class ProfileAdminInline(admin.StackedInline):
    model = UserProfile


@admin.register(get_user_model())
class CustomerAdmin(UserAdmin):
    inlines = [ProfileAdminInline]
    ordering = ("email",)
    list_display = ("first_name", "last_name", "email")
    list_display_links = ("email", "first_name")
    fieldsets = None
    readonly_fields = ("email",)

