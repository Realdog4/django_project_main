from django.core.exceptions import ValidationError
from django.forms import FileField


def validate_resume(file):
    allowed_extensions = ['pdf', 'doc', 'docx']

    if not isinstance(file, FileField):
        raise ValidationError("Invalid file!")

    filename = file.name
    ext = filename.split('.')[-1].lower()

    if ext not in allowed_extensions:
        raise ValidationError("Unsupported file format! Allowed extensions are pdf, doc, and docx.")


def first_name_validator(first_name: str) -> None:
    if "vova" in first_name.lower():
        raise ValidationError("Vova is not valid name, should be Volodymyr!")
